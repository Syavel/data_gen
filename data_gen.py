from PIL import Image, ImageDraw, ImageFont, ImageDraw
import json
import random
import time
import os
import glob


#Parameters
random.seed(142)
datagen = 10000
validgen = 1000
#Size of the image (x,x)
output_size = 288
#Proximity option : decide if champions are likely to be superposed often or not (0 is random / 1 is max) (2nd parameter is between 0 and 1 and define the distance of close champs)
proximity = (0.25,0.2)
#Minimap icons dir
icons_dir = "map/icons/"
#Probability json file (should be a dict of list) / if no probability so base_probs will be used
probability_file = "map/proba.json"
#Number of champs by image proba (10 = [0,0,0,0,0,0,0,0,0,0,1])
champs_by_image = [0,0,0,0,0,0,0,1,1,2,10]
#Contouring of champs 
countouring = [(249,5,0),(50,154,221)]
#Quality
myquality = 70

#Populating icons_probabilities
base_probs = [20,45,30,5]
with open(probability_file, "r") as pf :
    icons_probabilities = json.load(pf)
for f in glob.glob(icons_dir+"*.png") :
    try :
        if icons_probabilities[f] :
            pass
    except KeyError :
        icons_probabilities[f] = base_probs


#Get all champs name
with open("champions.json","r") as champs :
    champions = json.load(champs)
nb_champs = len(champions)
#nb_champs = 20

#Get map image
minimap = Image.open("map/map_full_icons.png").resize((512,512))

current_dir = os.getcwd()
files_name = os.listdir(current_dir+"/"+icons_dir)
icons_list = list()
for icon in files_name :
    icons_list.append(icons_dir+icon)

champions_count = dict()
champion_base_size = (40,40)

#Get all champs images
champions_images = dict()
with open("names.txt","w+") as names :
    for i,ck in enumerate(champions):
        champions_images[i] = Image.open("champs/"+str(ck[0]).lower()+".png").resize(champion_base_size)
        champions_count[i] = 0
        names.write(str(ck[0])+"\n")

cwidth = champions_images[0].width
cheight = champions_images[0].height

#Defining functions
def getPositions(proximity, nb_champs, size):
    positions = list()
    for _ in range(nb_champs):
        isclose = random.choices([True,False],[proximity[0],1-proximity[0]])
        if isclose and positions :
            pcloseto = positions[random.randrange(0,len(positions))]
            positions.append((random.randint(int(max(pcloseto[0]-proximity[1]*size[0],0)),int(min(pcloseto[0]+proximity[1]*size[0],size[0]))),random.randint(int(max(pcloseto[1]-proximity[1]*size[1],0)),int(min(pcloseto[1]+proximity[1]*size[0],size[1])))))
        else :
            positions.append((random.randint(0,size[0]),random.randint(0,size[1])))
    return positions

def creatingSet(settype, dirset, maxgen):
    tstart = time.time()
    print("Creating set : {} / time : {}".format(settype,tstart))

    with open(settype+".txt","w+") as datafile :
        for i in range(maxgen) :
            new_image = minimap.copy()
            #Give x champs indexes based on champs_by_image
            some_champs = random.sample(range(nb_champs), random.choices(range(len(champs_by_image)),champs_by_image)[0])
            #Summing occurences for each champ
            for c in some_champs :
                champions_count[c] +=1
            #Give center coordinates
            positions = getPositions(proximity, len(some_champs), (minimap.width-cwidth,minimap.height-cheight))

            with open(dirset+"/map"+str(i)+".txt", "w+") as txtfile :
                for position , champ in zip(positions, some_champs) :
                    draw = ImageDraw.Draw(new_image)
                    draw.ellipse([(position[0]-1,position[1]-1),(position[0]+41,position[1]+41)], fill=random.choice(countouring), outline=None)
                    new_image.alpha_composite(champions_images[champ], dest=position)
                    txtfile.write(str(champ)+" "+str((position[0]+champion_base_size[0]/2)/minimap.width)+" "+str((position[1]+champion_base_size[1]/2)/minimap.height)+" "+str(cwidth/minimap.width)+" "+str(cheight/minimap.height)+"\n")

            #Add some random icons
            icon_number = random.choices([0,1,2,3,4,5,6,7],[3,5,10,25,20,15,10,5])
            for _ in range(icon_number[0]) :
                icon = random.choice(icons_list)
                position = (random.randint(0,minimap.width-cwidth),random.randint(0,minimap.height-cheight))
                iconI = Image.open(icon)
                new_image.alpha_composite(iconI, dest=position)

            new_image = new_image.convert("RGB")
            new_image.resize((output_size,output_size)).save(dirset+"/map"+str(i)+".jpg","jpeg",quality=myquality)
        
            datafile.write("data_gen/"+dirset+"/map"+str(i)+".jpg\n")


    print("Created set : {} in {} seconds".format(settype,time.time()-tstart))

creatingSet("valid","valid_gen",validgen)
creatingSet("train","data_gen",datagen)

with open("count.json","w+") as count :
    json.dump(champions_count, count)